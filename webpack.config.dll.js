const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
console.log('NODE_ENV =', process.env.NODE_ENV);
//获取环境变量
const isProd = process.env.NODE_ENV == 'production';
module.exports = {
    mode: process.env.NODE_ENV,
    entry: {
        vue: ['vue'], //把三方库提前打包一下
        element_ui: ['element-ui'],
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: isProd ? '[name].dll.[hash:8].js' : '[name].dll.js',
        library: isProd ? '[name]_[hash:8]' : '[name]', //导出变量名
        // libraryTarget: 'umd2', //默认var, "var" | "assign" | "this" | "window" | "self" | "global" | "commonjs" | "commonjs2" | "commonjs-module" | "amd" | "amd-require" | "umd" | "umd2" | "jsonp" | "system"
    },
    plugins: [
        new webpack.DllPlugin({
            name: isProd ? '[name]_[hash:8]' : '[name]',
            path: path.resolve(__dirname, 'build', '[name]-manifest.json'),
        }),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: ['**/*', '!*.json'],
        }),
    ],
};
