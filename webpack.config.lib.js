const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
console.log('NODE_ENV =', process.env.NODE_ENV)
//获取环境变量
const isProd = process.env.NODE_ENV == 'production'
module.exports = {
    mode: process.env.NODE_ENV, //模式:development,production 写到命令行里面了
    entry: {
        myLib: './src/com/index.js', //入口文件
    },
    output: {
        filename: isProd ? 'dist/[name].min.js' : 'dist/[name].js', //'bundle.[hash:8].js', //生成的路径及文件名
        path: path.resolve(__dirname, 'library'), //导出目录，必须是绝对路径
        library: '[name]', //导出变量名
        libraryTarget: 'umd2', //默认var, "var" | "assign" | "this" | "window" | "self" | "global" | "commonjs" | "commonjs2" | "commonjs-module" | "amd" | "amd-require" | "umd" | "umd2" | "jsonp" | "system"
    },
    module: {
        rules: [
            {
                test: /\.js$/i,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                [
                                    '@babel/preset-env',
                                    {
                                        modules: false, // 默认值是auto,备选项： "amd" | "umd" | "systemjs" | "commonjs" | "cjs" | "auto" | false
                                        useBuiltIns: 'entry', //默认值是false,备选项： "usage" | "entry" | false
                                        //false：需要在 js 代码第一行主动 import '@babel/polyfill'，会将@babel/polyfill 整个包全部导入。
                                        // （不推荐，能覆盖到所有 API 的转译，但体积最大）
                                        // entry：需要在 js 代码第一行主动 import '@babel/polyfill'，会将 browserslist 环境不支持的所有垫片都导入。
                                        // （能够覆盖到'hello'.includes('h')这种句法，足够安全且代码体积不是特别大）
                                        // usage：项目里不用主动 import，会自动将代码里已使用到的、且 browserslist 环境不支持的垫片导入。
                                        // （但是检测不到'hello'.includes('h')这种句法，对这类原型链上的句法问题不会做转译，书写代码需注意）
                                        corejs: 3,
                                        // targets: [], //用来配置需要支持的的环境，不仅支持浏览器，还支持 node。如果没有配置 targets 选项，就会读取项目中的 browserslist 配置项。
                                        // loose: false //默认值是 false，如果 preset-env 中包含的 plugin 支持 loose 的设置，那么可以通过这个字段来做统一的设置。
                                    },
                                ],
                            ],
                            plugins: [
                                '@babel/plugin-transform-runtime', //减少babel中的helper重复问题
                            ],
                        },
                    },
                ],
                include: path.resolve(__dirname, 'src'), //包括
                exclude: /node_modules/, //排除node_modules目录
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/com/index.html',
            filename: 'index.html',
            meta: {},
            mode: isProd,
            minify: isProd,
            hash: !isProd, //文件后追加加hash值，不修改文件名
        }),
        new webpack.BannerPlugin('coding by leehoo'),
        // new CleanWebpackPlugin(),
    ], //放着所有的webpack插件
}
