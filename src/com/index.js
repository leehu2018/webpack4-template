// 可以将项目中优秀的组件或js提取出来，打包为一个库，供别处使用
// 这是一个库文件的入口文件，这里可以引入项目中用到的公共组件
// 推荐用commonjs方式输出库文件，esm方式输出会多一层default
// import xxx from './js/xxx.js'
const add = (a, b) => {
    return a + b
}

module.exports = { add }
