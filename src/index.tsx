// import 'core-js/stable';
// import 'regenerator-runtime/runtime';
import Vue from 'vue';
import ElementUI from 'element-ui';
import './scss/index.scss';
import leehoo from './js/a';
console.log(leehoo + '20223');

Vue.use(ElementUI, { size: 'small', zIndex: 3000 });

interface Params {
    name: string;
    age: number;
}

let ps = (p: Params) => {
    console.log(p.name + ':' + p.age);
};

ps({
    name: '张三',
    age: 12,
});

new Vue({
    el: '#app',
    data: {
        title: 'Hello world!',
    },
    methods: {
        init() {
            fetch('/getList')
                .then((res) => res.json())
                .then((data) => {
                    console.log(data);
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        },
    },
});

// 只有当开启了模块热替换时 module.hot 才存在
// if (module.hot) {
//   // accept 函数的第一个参数指出当前文件接受哪些子模块的替换，这里表示只接受 ./AppComponent 这个子模块
//   // 第2个参数用于在新的子模块加载完毕后需要执行的逻辑
//   module.hot.accept(['./js/a.js'], () => {
//     console.log('更新了');
//   });
// }
