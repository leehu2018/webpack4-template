import Vue from 'vue';
import leehoo from './js/a';
console.log(leehoo);

setTimeout(() => {
    import(/* webpackChunkName: "async_b.js" */ './js/b').then((module) => {
        console.log('3s => ', module.default);
    });
}, 3000);

new Vue({
    el: '#app',
    data: {
        title: 'Hello world!',
    },
    methods: {
        init() {
            fetch('/getList')
                .then((res) => res.json())
                .then((data) => {
                    console.log(data);
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        },
    },
});
