const FtpClient = require('ftp');
const fs = require('fs');
const dayjs = require('dayjs');
const client = new FtpClient();
const config = {
    host: '192.168.252.72',
    port: 10021,
    user: 'fuser',
    password: 'tmkj@zgb123',
    keepalive: 10000,
};
function connect() {
    return new Promise((resolve, reject) => {
        client.on('ready', () => {
            console.log('ftp ready');
            resolve();
        });
        client.on('close', () => {
            console.log('ftp close');
        });
        client.on('end', () => {
            console.log('ftp end');
        });
        client.on('error', (err) => {
            console.log('ftp err', err);
            reject(err);
        });
        client.connect(config);
    });
}
async function upload(sourcePath, targetPath) {
    if (!fs.existsSync(sourcePath)) {
        return false;
    }
    await connect();
    return new Promise((resolve, reject) => {
        client.put(sourcePath, targetPath, (err) => {
            client.end();
            if (err) {
                console.log(err);
                reject(false);
            } else {
                console.log(`upload file completed. filePath: ${targetPath}`);
                resolve(true);
            }
        });
    });
}
const sourcePath = `./release/dist.zip`;
const targetPath = `/dist_${dayjs().format('YYYYMMDDhhmmss')}.zip`;
upload(sourcePath, targetPath);
